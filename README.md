# Crossflow

A Python-based workflow system.

Crossflow allows you to build and execute complex workflows that chain together 
command-line driven tools.

For details, see the [Wiki](https://bitbucket.org/claughton/crossflow/wiki/Home).

### Authors:

Charlie Laughton [charles.laughton@nottingham.ac.uk](mailto:charles.laughton@nottingham.ac.uk)

Christian Suess
