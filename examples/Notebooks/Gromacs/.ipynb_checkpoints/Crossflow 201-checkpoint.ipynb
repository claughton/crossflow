{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "This notebook illustrates how to create workflows using *Crossflow*, and then run them efficiently using *Crossflow*'s distributed computing capabilities.\n",
    "\n",
    "It's assumed you have done the \"Crossflow 101\" notebook or similar, and understand how you create *tasks* to run command-line appplications.\n",
    "\n",
    " - If you are running the Notebook locally, you will need to have the following installed:\n",
    "   - Gromacs\n",
    "   - The python package MDTraj\n",
    "\n",
    "---\n",
    "\n",
    "The aim of this notebook is to build on the 'Crossflow 101\" notebook, and create a workflow to:\n",
    "\n",
    "1. Run a short MD job\n",
    "2. Energy minimise each of the structures in the resulting trajectory\n",
    "\n",
    "---\n",
    "\n",
    "Begin by creating the required tasks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from crossflow import filehandling, tasks, clients\n",
    "# Build tasks for mdrun and grompp:\n",
    "md = tasks.SubprocessTask('gmx mdrun -ntmpi 1 -s x.tpr -c x.gro -o x.trr -g x.log')\n",
    "md.set_inputs(['x.tpr'])\n",
    "md.set_outputs(['x.trr'])\n",
    "\n",
    "em = tasks.SubprocessTask('gmx mdrun -ntmpi 1 -s x.tpr -c x.gro')\n",
    "em.set_inputs(['x.tpr'])\n",
    "em.set_outputs(['x.gro'])\n",
    "\n",
    "grompp = tasks.SubprocessTask('gmx grompp -f x.mdp -c x.gro -p x.top -o x.tpr -maxwarn 1')\n",
    "grompp.set_inputs(['x.mdp', 'x.gro', 'x.top'])\n",
    "grompp.set_outputs(['x.tpr'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 1: Running the workflow without distributed computing\n",
    "\n",
    "The workflow is fairly simple: \n",
    "1. Run grompp to prepare the starting structure for MD.\n",
    "2. Run the MD.\n",
    "3. For each structure in the trajectory:\n",
    "\n",
    "    a. Run grompp to prepare it for energy minimisation.\n",
    "    \n",
    "    b. Run the energy minimisation.\n",
    "    \n",
    "    c. Save the final coordinates to a file.\n",
    "    \n",
    "Below we run each step interactively, i.e. without staring a crossflow client (this means that the objects returned by the functions are the actual data, not `Futures`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run grompp and the MD:\n",
    "fh = filehandling.FileHandler()\n",
    "start_crds = fh.load('bpti.gro')\n",
    "topfile = fh.load('bpti.top')\n",
    "md_mdp = fh.load('nvt.mdp')\n",
    "mdtpr = grompp(md_mdp, start_crds, topfile)\n",
    "trajectory = md(mdtpr)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert the MD trajectory file to an MDTraj trajectory object:\n",
    "import mdtraj as mdt\n",
    "traj = mdt.load(trajectory, top=start_crds)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the energy minimisation mdp file, then minimise each snapshot in turn:\n",
    "em_mdp = fh.load('em.mdp')\n",
    "for i, snapshot in enumerate(traj):\n",
    "    print('Energy minimising snapshot {}'.format(i))\n",
    "    emtpr = grompp(em_mdp, snapshot, topfile)\n",
    "    mincrds = em(emtpr)\n",
    "    mincrds.save('bpti_min_{}.gro'.format(i))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 2: Running the workflow with distributed computing\n",
    "\n",
    "In distributed computing, tasks are farmed out to \"workers\". Where the program logic permits, tasks that can be run in parallel are sent to different workers. Clearly that applies to the energy minimisation steps here - they are independent of each other, and if enough workers were available, each task could be run at the same time.\n",
    "\n",
    "Crossflow comes with a distributed computing capability built on [dask.distributed](http://distributed.dask.org/en/latest/). If you are running this notebook on your own desktop machine or equivalent, it will create a \"pool\" of workers on it to run jobs in parallel. Depending on the capabilities of your machine, you may or may not see much performance improvement compared to running the jobs without distributed computing, but if you have the resources to add the code to launch a \"proper\" cluster (see e.g. [dask-kubernetes](https://kubernetes.dask.org/en/latest/) or [dask-jobqueue](https://jobqueue.dask.org/en/latest/) then each worker is a separate compute node and you should see a significant speed-up.\n",
    "\n",
    "----\n",
    "\n",
    "Begin by creating a *client*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# In a production setting you would have extra code here to create a 'proper' distributed cluster:\n",
    "# cluster = ???\n",
    "# \n",
    "cluster = None\n",
    "client = clients.Client(cluster)\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You are going to distribute the energy minimisations of the snapshots from the trajectory file across the workers in your cluster. For performance reasons, you begin by uploading the snaphots to the cluster, using `client.upload()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "snapshots = [client.upload(t) for t in traj]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you will run the grompp and mdrun jobs in parallel across the available workers, using the `client.map()` method. This takes the name of the task as the first argument, and *lists* of task arguments after that. The map() function takes one item from each of the argument lists, and evaluates the task using those. It then returns a *list* of task outputs.\n",
    "\n",
    "Thus, if a task had the form:\n",
    "\n",
    "    result = myfunc(inputa, inputb)\n",
    "\n",
    "Then this would become:\n",
    "\n",
    "    [result1, result2] = client.map(myfunc, [inputa_1, inputa_2], [inputb_1, inputb_2])\n",
    "\n",
    "However as a short-cut, if one of the arguments (e.g. inputb) is always the same, you can instead write:\n",
    "\n",
    "    [result1, result2] = client.map(myfunc, [inputa_1, inputa_2], inputb)\n",
    "    \n",
    "And `input_b` will be expanded to `[input_b, input_b]` automatically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the grompp jobs, then the energy minimisations:\n",
    "em_tprs = client.map(grompp, em_mdp, snapshots, topfile) # Note only snapshots is a list, other arguments get expanded automatically\n",
    "mincrds = client.map(em, em_tprs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may have been surprised that when you executed the cell above, it appeared to complete almost instantaneously - did the jobs really run that fast? \n",
    "\n",
    "No - the `client.map()` method runs the jobs asynchronously - they have been submitted to the workers, but probably have not finished yet. The variables `em_tprs` and `mincrds` are not actually the (lists of) new files - they are `futures` from which, as some time in the future, the real files can be obtained by calling their `result()` method.\n",
    "\n",
    "In the cell below you wait for the jobs to complete, and then write out the minimized coordinate files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, mincrd in enumerate(mincrds):\n",
    "    print('saving minimised snapshot {}'.format(i))\n",
    "    mincrd.result().save('bpti_min_{}.gro'.format(i))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's recommended to properly shut down the client before you quit the notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "client.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
